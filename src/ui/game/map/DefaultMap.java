package ui.game.map;

import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.scene.image.ImageView;

public class DefaultMap {
	
	@FXML // Auvergne-Rh�ne-Alpes #0
    public ImageView D03, D63, D15, D43, D42, D69, D01, D74, D73, D38, D26, D07;

    @FXML // Bourgogne-Franche-Compt� #1
    public ImageView D89, D58, D21, D71, D39, D25, D70, D90;

    @FXML //Bretagne #2
    public ImageView D29, D22, D56, D35;

    @FXML // Centre-Val de Loire #3
    public ImageView D37, D36, D18, D41, D28, D45;

    @FXML // Corse #4
    public ImageView D2B, D2A;

    @FXML // Grand Est #5
    public ImageView D08, D51, D10, D52, D88, D54, D55, D57, D67, D68;
    
    @FXML // Hauts-deFrance #6
    public ImageView D62, D59, D80, D02, D60;
    
    @FXML // �le-de-France #7
    public ImageView D95, D78, D91, D77, D75;
    
    @FXML // Normandie #8
    public ImageView D50, D14, D61,D27, D76;
    
    @FXML // Nouvelle-Aquitaine #9
    public ImageView D79, D86, D87, D23, D19, D24, D16, D17, D33, D47, D40, D64;
    
    @FXML // Occitanie #10
    public ImageView D46, D12, D48, D30, D34, D81, D82, D32, D31, D11, D66, D09, D65;
    
    @FXML // Pays de la Loire #11
    public ImageView D53, D72, D49, D44, D85;
    
    @FXML // Provence-Alpes-C�te d'Azur #12
    public ImageView D84, D05, D04, D06, D83, D13;
    
    public DefaultMap(ImageView D01, ImageView D02, ImageView D03, ImageView D04, ImageView D05, ImageView D06, ImageView D07, ImageView D08, ImageView D09, ImageView D10, ImageView D11, ImageView D12, ImageView D13, ImageView D14, ImageView D15, ImageView D16, ImageView D17, ImageView D18, ImageView D19, ImageView D2A, ImageView D2B, ImageView D21, ImageView D22, ImageView D23, ImageView D24, ImageView D25, ImageView D26, ImageView D27, ImageView D28, ImageView D29, ImageView D30, ImageView D31, ImageView D32, ImageView D33, ImageView D34, ImageView D35, ImageView D36, ImageView D37, ImageView D38, ImageView D39, ImageView D40, ImageView D41, ImageView D42, ImageView D43, ImageView D44, ImageView D45, ImageView D46, ImageView D47, ImageView D48, ImageView D49, ImageView D50, ImageView D51, ImageView D52, ImageView D53, ImageView D54, ImageView D55, ImageView D56, ImageView D57, ImageView D58, ImageView D59, ImageView D60, ImageView D61, ImageView D62, ImageView D63, ImageView D64, ImageView D65, ImageView D66, ImageView D67, ImageView D68, ImageView D69, ImageView D70, ImageView D71, ImageView D72, ImageView D73, ImageView D74, ImageView D75, ImageView D76, ImageView D77, ImageView D78, ImageView D79, ImageView D80, ImageView D81, ImageView D82, ImageView D83, ImageView D84, ImageView D85, ImageView D86, ImageView D87, ImageView D88, ImageView D89, ImageView D90, ImageView D91, ImageView D92, ImageView D93, ImageView D94, ImageView D95){
    	this.D01= D01;
    	this.D02= D02;
    	this.D03= D03;
    	this.D04= D04;
    	this.D05= D05;
    	this.D06= D06;
    	this.D07= D07;
    	this.D08= D08;
    	this.D09= D09;
    	this.D10= D10;
    	this.D11= D11;
    	this.D12= D12;
    	this.D13= D13;
    	this.D14= D14;
    	this.D15= D15;
    	this.D16= D16;
    	this.D17= D17;
    	this.D18= D18;
    	this.D19= D19;
    	this.D2A= D2A;
    	this.D2B= D2B;
    	this.D21= D21;
    	this.D22= D22;
    	this.D23= D23;
    	this.D24= D24;
    	this.D25= D25;
    	this.D26= D26;
    	this.D27= D27;
    	this.D28= D28;
    	this.D29= D29;
    	this.D30= D30;
    	this.D31= D31;
    	this.D32= D32;
    	this.D33= D33;
    	this.D34= D34;
    	this.D35= D35;
    	this.D36= D36;
    	this.D37= D37;
    	this.D38= D38;
    	this.D39= D39;
    	this.D40= D40;
    	this.D41= D41;
    	this.D42= D42;
    	this.D43= D43;
    	this.D44= D44;
    	this.D45= D45;
    	this.D46= D46;
    	this.D47= D47;
    	this.D48= D48;
    	this.D49= D49;
    	this.D50= D50;
    	this.D51= D51;
    	this.D52= D52;
    	this.D53= D53;
    	this.D54= D54;
    	this.D55= D55;
    	this.D56= D56;
    	this.D57= D57;
    	this.D58= D58;
    	this.D59= D59;
    	this.D60= D60;
    	this.D61= D61;
    	this.D62= D62;
    	this.D63= D63;
    	this.D64= D64;
    	this.D65= D65;
    	this.D66= D66;
    	this.D67= D67;
    	this.D68= D68;
    	this.D69= D69;
    	this.D70= D70;
    	this.D71= D71;
    	this.D72= D72;
    	this.D73= D73;
    	this.D74= D74;
    	this.D75= D75;
    	this.D76= D76;
    	this.D77= D77;
    	this.D78= D78;
    	this.D79= D79;
    	this.D80= D80;
    	this.D81= D81;
    	this.D82= D82;
    	this.D83= D83;
    	this.D84= D84;
    	this.D85= D85;
    	this.D86= D86;
    	this.D87= D87;
    	this.D88= D88;
    	this.D89= D89;
    	this.D90= D90;
    	this.D91= D91;
    	//this.D92= D92;
    	//this.D93= D93;
    	//this.D94= D94;
    	this.D95= D95;

    }
    
    public ArrayList<GUITerritory> getTerritoryList(){
		ArrayList<GUITerritory> list = new ArrayList<GUITerritory>();
		list.add(new GUITerritory("Ain","01", 0, 0,D01));
		list.add(new GUITerritory("Aisne","02", 1,6,D02));
		list.add(new GUITerritory("Allier","03", 2, 0,D03));
		list.add(new GUITerritory("Alpes-de-Haute-Provence","04", 3, 12,D04));
		list.add(new GUITerritory("Hautes-Alpes","05", 4, 12,D05));
		list.add(new GUITerritory("Alpes-Maritimes","06", 5, 12,D06));
		list.add(new GUITerritory("Ard�che","07", 6, 0,D07));
		list.add(new GUITerritory("Ardennes","08", 7, 5,D08));
		list.add(new GUITerritory("Ari�ge","09", 8, 10,D09));
		list.add(new GUITerritory("Aube","10", 9, 5,D10));
		list.add(new GUITerritory("Aude","11", 10, 10,D11));
		list.add(new GUITerritory("Aveyron","12", 11, 10,D12));
		list.add(new GUITerritory("Bouches-du-Rh�ne","13", 12, 12,D13));
		list.add(new GUITerritory("Calvados","14", 13, 8,D14));
		list.add(new GUITerritory("Cantal","15", 14, 0,D15));
		list.add(new GUITerritory("Charente","16", 15, 9,D16));
		list.add(new GUITerritory("Charente-Maritime","17", 16, 9,D17));
		list.add(new GUITerritory("Cher","18", 17, 3,D18));
		list.add(new GUITerritory("Corr�ze","19", 18, 9,D19));
		list.add(new GUITerritory("Corse-du-Sud","2A", 19, 4,D2A));
		list.add(new GUITerritory("Haute-Corse","2B", 20, 4,D2B));
		list.add(new GUITerritory("C�te-d'Or","21", 21, 1,D21));
		list.add(new GUITerritory("C�tes d'Armor","22", 22, 2,D22));
		list.add(new GUITerritory("Creuse","23", 23, 9,D23));
		list.add(new GUITerritory("Dordogne","24", 24, 9,D24));
		list.add(new GUITerritory("Doubs","25", 25, 1,D25));
		list.add(new GUITerritory("Dr�me","26", 26, 0,D26));
		list.add(new GUITerritory("Eure","27", 27, 8,D27));
		list.add(new GUITerritory("Eure-et-Loir","28", 28, 3,D28));
		list.add(new GUITerritory("Finist�re","29", 29, 2,D29));
		list.add(new GUITerritory("Gars","30", 30, 10,D30));
		list.add(new GUITerritory("Haute-Garonne","31", 31, 10,D31));
		list.add(new GUITerritory("Gers","32", 32, 10,D32));
		list.add(new GUITerritory("Gironde","33", 33, 9,D33));
		list.add(new GUITerritory("H�rault","34", 34, 10,D34));
		list.add(new GUITerritory("Ille-et-Vilaine","35", 35, 2,D35));
		list.add(new GUITerritory("Indre","36", 36, 3,D36));
		list.add(new GUITerritory("Indre-et-Loire","37", 37, 3,D37));
		list.add(new GUITerritory("Is�re","38", 38, 0,D38));
		list.add(new GUITerritory("Jura","39", 39, 1,D39));
		list.add(new GUITerritory("Landes","40", 40, 9,D40));
		list.add(new GUITerritory("Loir-et-Cher","41", 41, 3,D41));
		list.add(new GUITerritory("Loire","42", 42, 0,D42));
		list.add(new GUITerritory("Haute-Loire","43", 43, 0,D43));
		list.add(new GUITerritory("Loire-Atlantique","44", 44, 11,D44));
		list.add(new GUITerritory("Loiret","45", 45, 3,D45));
		list.add(new GUITerritory("Lot","46", 46, 10,D46));
		list.add(new GUITerritory("Lot-et-Garonne","47", 47, 9,D47));
		list.add(new GUITerritory("Loz�re","48", 48, 10,D48));
		list.add(new GUITerritory("Maine-et-Loire","49", 49, 11,D49));
		list.add(new GUITerritory("Manche","50", 50, 8,D50));
		list.add(new GUITerritory("Marne","51", 51, 5,D51));
		list.add(new GUITerritory("Haute-Marne","52",52, 5,D52));
		list.add(new GUITerritory("Mayenne","53", 53, 11,D53));
		list.add(new GUITerritory("Meurthe-et-Moselle","54", 54, 5,D54));
		list.add(new GUITerritory("Meuse","55", 55, 5,D55));
		list.add(new GUITerritory("Morbihan","56", 56, 2,D56));
		list.add(new GUITerritory("Moselle","57", 57, 5,D57));
		list.add(new GUITerritory("Ni�vre","58", 58, 1,D58));
		list.add(new GUITerritory("Nord","59", 59, 6,D59));
		list.add(new GUITerritory("Oise","60", 60, 6,D60));
		list.add(new GUITerritory("Orne","61", 61, 8,D61));
		list.add(new GUITerritory("Pas-de-Calais","62", 62, 6,D62));
		list.add(new GUITerritory("Puy-de-D�me","63",63, 0,D63));
		list.add(new GUITerritory("Pyr�n�es-Atlantiques","64", 64, 9,D64));
		list.add(new GUITerritory("Hautes-Pyr�n�es","65", 65, 10,D65));
		list.add(new GUITerritory("Pyr�n�es-Orientales","66", 66, 10,D66));
		list.add(new GUITerritory("Bas-Rhin","67", 67, 5,D67));
		list.add(new GUITerritory("Haut-Rhin","68",68, 5,D68));
		list.add(new GUITerritory("Rh�ne","69",69, 0,D69));
		list.add(new GUITerritory("Haute-Sa�ne","70",70, 1,D70));
		list.add(new GUITerritory("Sa�ne-et-Loire","71",71, 1,D71));
		list.add(new GUITerritory("Sarthe","72",72, 11,D72));
		list.add(new GUITerritory("Savoie","73",73, 0,D73));
		list.add(new GUITerritory("Haute-Savoie","74",74, 0,D74));
		list.add(new GUITerritory("Paris","75",75, 7,D75));
		list.add(new GUITerritory("Seine-Maritime","76",76, 8,D76));
		list.add(new GUITerritory("Seine-et-Marne","77",77, 7,D77));
		list.add(new GUITerritory("Yvelines","78",78, 7,D78));
		list.add(new GUITerritory("Deux-S�vres","79",79, 9,D79));
		list.add(new GUITerritory("Somme","80",80, 6,D80));
		list.add(new GUITerritory("Tarn","81",81, 10,D81));
		list.add(new GUITerritory("Tarn-et-Garonne","82",82, 10,D82));
		list.add(new GUITerritory("Var","83",83, 12,D83));
		list.add(new GUITerritory("Vaucluse","84",84, 12,D84));
		list.add(new GUITerritory("Vend�e","85",85, 11,D85));
		list.add(new GUITerritory("Vienne","86", 86, 9,D86));
		list.add(new GUITerritory("Haute-Vienne","87",87, 9,D87));
		list.add(new GUITerritory("Vosges","88", 88, 5,D88));
		list.add(new GUITerritory("Yonne","89", 89, 1,D89));
		list.add(new GUITerritory("Territoire de Belfort","90",90, 1,D90));
		list.add(new GUITerritory("Essonne","91", 91, 7,D91));
		//list.add(new GUITerritory("Hauts-de-Seine","92", 7,D92));
		//list.add(new GUITerritory("Seine-St-Denis","93",7,D93));
		//list.add(new GUITerritory("Val-de-Marn,"9e"4",7,D94));
		list.add(new GUITerritory("Val-D'Oise","95",92, 7,D95));

		return list;
	}
}
