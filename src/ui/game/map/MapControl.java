package ui.game.map;

import java.io.IOException;
import java.util.*;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.*;

public class MapControl extends Pane {

	public enum ArmyClass {
		None, Infantry, Cavalry, Artillery
	}

	Map<Integer, GUITerritory> territoryByID = new HashMap<>();
	Map<ImageView, GUITerritory> territoryByImageView = new HashMap<>();

	Map<Integer, Image> artilleryImages = new HashMap<>();
	Map<Integer, Image> infantryImages = new HashMap<>();
	Map<Integer, Image> cavalryImages = new HashMap<>();

	@FXML
	ImageView worldmap;
	@FXML
	ImageView D01, D02, D03, D04, D05, D06, D07, D08, D09, D10,
	D11, D12, D13, D14, D15, D16, D17, D18, D19, D2A,
	D2B, D21, D22, D23, D24, D25, D26, D27, D28, D29,
	D30, D31, D32, D33, D34, D35, D36, D37, D38, D39,
	D40, D41, D42, D43, D44, D45, D46, D47, D48, D49,
	D50, D51, D52, D53, D54, D55, D56, D57, D58, D59,
	D60, D61, D62, D63, D64, D65, D66, D67, D68, D69,
	D70, D71, D72, D73, D74, D75, D76, D77, D78, D79,
	D80, D81, D82, D83, D84, D85, D86, D87, D88, D89,
	D90, D91, D92, D93, D94, D95;
	@FXML
	Text departmentLibelle;

	ArrayList<GUITerritory> clickableTerritories;

	private GUITerritory selectedFrom, selectedTo;

	public MapControl() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
				"MapControl.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	public void initialise() {
		loadArmyIcons();

		DefaultMap territories = new DefaultMap(D01, D02, D03, D04, D05, D06, D07, D08, D09, D10,
				D11, D12, D13, D14, D15, D16, D17, D18, D19, D2A,
				D2B, D21, D22, D23, D24, D25, D26, D27, D28, D29,
				D30, D31, D32, D33, D34, D35, D36, D37, D38, D39,
				D40, D41, D42, D43, D44, D45, D46, D47, D48, D49,
				D50, D51, D52, D53, D54, D55, D56, D57, D58, D59,
				D60, D61, D62, D63, D64, D65, D66, D67, D68, D69,
				D70, D71, D72, D73, D74, D75, D76, D77, D78, D79,
				D80, D81, D82, D83, D84, D85, D86, D87, D88, D89,
				D90, D91, D92, D93, D94, D95);

		clickableTerritories = territories.getTerritoryList();

		Label label;
		for (final GUITerritory territory : clickableTerritories) {
			// Add territory
			territoryByID.put(territory.getId(), territory);
			territoryByImageView.put(territory.getImage(), territory);

			// Mouse events
			addMouseHoverEvents(territory);

			// Army
			label = new Label();
			territory.setArmyLabel(label);
			getChildren().add(label);
		}
	}

	void loadArmyIcons() {
		for (int i = 1; i < 7; i++) {
			artilleryImages.put(
					i,
					new Image(getClass().getResourceAsStream(
							"army/artillery_player" + i + ".png")));
			infantryImages.put(
					i,
					new Image(getClass().getResourceAsStream(
							"army/infantry_player" + i + ".png")));
			cavalryImages.put(
					i,
					new Image(getClass().getResourceAsStream(
							"army/cavalry_player" + i + ".png")));
		}
	}

	void addMouseHoverEvents(GUITerritory territory) {
		System.out.println(territory.getDepartmentNumber());
		System.out.println(territory.getImage());
		territory.getImage().addEventFilter(MouseEvent.MOUSE_ENTERED,
				new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent mouseEvent) {
						if (territory.getImage().getOpacity() < 100)
							territory.getImage().setOpacity(100);
						
						departmentLibelle.setText(territory.getDepartmentNumber() + " : " + territory.getName());
					}
				});

		territory.getImage().addEventFilter(MouseEvent.MOUSE_EXITED,
				new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent mouseEvent) {
						if (MapControl.this.getSelectedFrom() != territory
								&& MapControl.this.getSelectedTo() != territory)
							territory.getImage().setOpacity(0);
						departmentLibelle.setText("");
					}
				});
	}

	public void updateTerritory(int playerID, int numberOfArmies,
			GUITerritory territory) {

		ArmyClass oldClass = territory.getArmyClass();
		int oldOwnerID = territory.getOwnerID();
		int oldNumberOfArmies = territory.getNumberOfArmies();

		territory.setOwnerID(playerID);
		territory.setNumberOfArmies(numberOfArmies);

		Label label = territory.getArmyLabel();

		// Update image (if required)
		updateTerritoryImage(territory, label, oldOwnerID, oldClass);

		// Update number
		if (territory.getNumberOfArmies() != oldNumberOfArmies) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					label.setText(Integer.toString(numberOfArmies));
				}
			});
		}
	}

	void updateTerritoryImage(GUITerritory territory, Label label,
			int oldOwnerID, ArmyClass oldClass) {
		if (territory.getOwnerID() != oldOwnerID
				|| territory.getArmyClass() != oldClass) {

			ImageView image = null;
			int labelOffset = 0;

			switch (territory.getArmyClass()) {
			case Artillery:
				image = new ImageView(artilleryImages.get(territory
						.getOwnerID() + 1));
				labelOffset = -162;
				break;
			case Cavalry:
				image = new ImageView(
						cavalryImages.get(territory.getOwnerID() + 1));
				labelOffset = -110;
				break;
			case Infantry:
				image = new ImageView(
						infantryImages.get(territory.getOwnerID() + 1));
				labelOffset = -94;
				break;
			default:
				image = null;
				break;
			}

			double x = territory.getImage().getLayoutX()
					+ territory.getImage().getImage().getWidth() / 2;
			double y = territory.getImage().getLayoutY()
					+ territory.getImage().getImage().getHeight() / 2;

			if (image != null) {
				image.setScaleX(0.25f);
				image.setScaleY(0.25f);

				if (image.getImage() != null) {
					x -= image.getImage().getWidth() / 2;
					y -= image.getImage().getHeight() / 2;
				}
			}

			final ImageView finalImage = image;
			final int finalLabelOffset = labelOffset;
			final double xF = x;
			final double yF = y;

			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					if (finalImage != null)
						label.setGraphic(finalImage);
					label.setGraphicTextGap(finalLabelOffset);

					label.setContentDisplay(ContentDisplay.TOP);
					label.setMouseTransparent(true);

					label.relocate(xF, yF);
				}
			});
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
			}
		}
	}

	public ArrayList<GUITerritory> getClickableTerritories() {
		return clickableTerritories;
	}

	public GUITerritory getTerritoryByID(int id) {
		return territoryByID.get(id);
	}

	public GUITerritory getSelectedFrom() {
		return selectedFrom;
	}

	public void setSelectedFrom(GUITerritory selectedFrom) {
		if (this.selectedFrom != null) {
			this.selectedFrom.setSelected(false);
		}

		if (selectedFrom != null) {
			selectedFrom.setSelected(true);
		}

		this.selectedFrom = selectedFrom;
	}

	public GUITerritory getSelectedTo() {
		return selectedTo;
	}

	public void setSelectedTo(GUITerritory selectedTo) {
		if (this.selectedTo != null) {
			this.selectedTo.setSelected(false);
		}

		if (selectedTo != null) {
			selectedTo.setSelected(true);
		}

		this.selectedTo = selectedTo;
	}
}
