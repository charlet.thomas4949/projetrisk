package ui.commandline;

import ai.AgentFactory;
import ai.AgentTypes;
import ai.agents.Agent;
import lobby.LocalGameLobby;
import lobby.RemoteGameLobby;
import lobby.handler.HostLobbyEventHandler;
import lobby.handler.JoinLobbyEventHandler;
import logic.Game;
import logic.state.Deck;
import networking.LobbyClient;
import networking.LocalPlayerHandler;
import player.IPlayer;
import settings.Settings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Adam on 31/01/2015.
 */
public class CLIMain {

    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("'Heberger' ou 'rejoindre' une partie");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        LocalGameLobby lobby;

        String value = reader.readLine();
        if(value.equals("host")) {
            lobby = hostLobby("Host Player Name");
            ourPlayerid = LocalGameLobby.HOST_PLAYERID;
            value = reader.readLine();
            if(value.equals("start")) {
                System.out.println("Requete de demarrage partie");
                lobby.startGame();
            }
            lobby.join();

        } else if(value.equals("join")) {
            joinLobby();
        }

        playGame();
    }

    static List<IPlayer> playersBefore = null;
    static List<IPlayer> playersAfter = null;

    static LocalPlayerHandler localHandler = null;
    static int ourPlayerid;

    public static void setPlayers(List<IPlayer> before, List<IPlayer> after, LocalPlayerHandler localPlayerHandler) {
        playersBefore = before;
        playersAfter = after;
        localHandler = localPlayerHandler;
    }

    private static void playGame() {
        if(playersBefore == null || playersAfter == null || localHandler == null) {
            System.out.println("erreur de parametrage du jeu.");
            return;
        }
        System.out.println("demmarage du jeu");
        Agent agent = AgentFactory.buildAgent(AgentTypes.Type.GREEDY);
        IPlayer localPlayer = new CommandLinePlayer(agent, new Scanner(System.in), new PrintWriter(System.out), "Moi", ourPlayerid);

        List<IPlayer> players = new LinkedList<>();
        players.addAll(playersBefore);
        players.add(localPlayer);
        players.addAll(playersAfter);

        Game game = new Game(players, localHandler, null);

        List<String> names = namePlayers(playersBefore, playersAfter);
        System.out.println("joueurs: ");
        for(String name : names) {
            System.out.println(name);
        }

        game.run();
    }

    private static List<String> namePlayers(List<IPlayer> playersBefore, List<IPlayer> playersAfter) {
        int i=0;
        List<String> names = new LinkedList<>();

        for(;i<playersBefore.size(); i++) {
            names.add("Joueur réseau " + i);
        }
        names.add("Joueur local");

        for(int j=0;j<playersAfter.size(); j++, i++) {
            names.add("NetworkPlayer " + i);
        }

        return names;
    }

    public static LocalGameLobby hostLobby(String name) {
        LocalGameLobby lobby = new LocalGameLobby(handler, Settings.DEFAULT_PORT, name);
        lobby.start();
        return lobby;
    }

    public static HostLobbyEventHandler handler = new HostLobbyEventHandler() {
        @Override
        public String onPlayerJoinRequest(LobbyClient client) {
            System.out.println("demande pour rejoindre partie de " + client.supportedVersions[0]);
            return null; // Accept the player (rejecting requires a string for reject message)
        }

        @Override
        public void onPlayerJoin(int playerid, String playerName) {
            System.out.println("joueur (" + playerid + "): " + playerName + " rejoint la partie.");
        }

        @Override
        public void onPlayerLeave(int playerid) {
            System.out.println("joueur " + playerid + " quitte la partie.");
        }

        @Override
        public void onPingStart() {
            System.out.println("ping envoyé ");
        }

        @Override
        public void onPingReceive(int playerid) {
            System.out.println("ping reçu de " + playerid);
        }

        @Override
        public void onReady() {
            System.out.println("prêt ");
        }

        @Override
        public void onReadyAcknowledge(int playerid) {
            System.out.println("sur accusé de réception prêt " + playerid);
        }

        @Override
        public void onInitialiseGame(double protocolVersion, String[] extendedFeatures) {

        }

        @Override
        public void onDicePlayerOrder() {
            System.out.println("sur l'ordre du joueur de dés ");
        }

        @Override
        public void onDiceHash(int playerid) {
            System.out.println("sur règle de dés " + playerid);
        }

        @Override
        public void onDiceNumber(int playerid) {
            System.out.println("sur nombre des dés " + playerid);
        }

        @Override
        public void onDiceCardShuffle() {
            System.out.println("sur le mélange de cartes de dés ");
        }

        @Override
        public void onLobbyComplete(List<IPlayer> playersBefore, List<IPlayer> playersAfter, Deck deck, LocalPlayerHandler localPlayerHandler) {
            System.out.println("Salon complet: ");
            System.out.println("\tJoueurs: " + playersBefore.toString());
            System.out.println("\tJoueurs: " + playersAfter.toString());

            System.out.println("À ce stade, nous devons transmettre ces données à la boucle de jeu");

            setPlayers(playersBefore, playersAfter, localPlayerHandler);
        }

        @Override
        public void onFailure(Throwable e) {
            System.out.println("en cas d'échec: " + e.getMessage());

            e.printStackTrace();
        }
    };

    public static void joinLobby() {
        try {
            RemoteGameLobby lobby = new RemoteGameLobby(InetAddress.getByName("127.0.0.1"), Settings.DEFAULT_PORT, joinHandler, "Player Name");

            lobby.start();

            lobby.join();
        } catch(UnknownHostException e) {
            System.out.println("Hébergeur inconnu: " + e.getMessage());
        } catch(InterruptedException e) {
            System.out.printf("Exception interrompue: " + e.getMessage());
        }
    }

    public static JoinLobbyEventHandler joinHandler = new JoinLobbyEventHandler() {

        @Override
        public void onTCPConnect() {
            System.out.println("sur Connection TCP ");
        }

        @Override
        public void onJoinAccepted(int playerid) {
            System.out.println("reqûete pour rejoindre partie de " + playerid+" acceptée");
            CLIMain.ourPlayerid = playerid;
        }

        @Override
        public void onJoinRejected(String message) {
            System.out.println("reqûete pour rejoindre partie refusée " + message);
        }

        @Override
        public void onPlayerJoin(int playerid, String playerName) {
            System.out.println("le joueur (" + playerid + "): " + playerName+ " rejoint la partie");
        }

        @Override
        public void onPlayerLeave(int playerid) {
            System.out.println("le joueur " + playerid+ " quitte la partie");
        }

        @Override
        public void onPingStart() {
            System.out.println("requête de ping démarre ");
        }

        @Override
        public void onPingReceive(int playerid) {
            System.out.println("requête de ping reçu " + playerid);
        }

        @Override
        public void onReady() {
            System.out.println("prêt ");
        }

        @Override
        public void onReadyAcknowledge(int playerid) {
            System.out.println("sur accusé de réception prêt " + playerid);
        }

        @Override
        public void onInitialiseGame(double protocolVersion, String[] extendedFeatures) {
            System.out.println("Initialisation de la partie " + protocolVersion + ". " + extendedFeatures.toString());
        }

        @Override
        public void onDicePlayerOrder() {
            System.out.println("sur l'ordre du joueur de dés ");
        }

        @Override
        public void onDiceHash(int playerid) {
            System.out.println("sur règle de dés " + playerid);
        }

        @Override
        public void onDiceNumber(int playerid) {
            System.out.println("sur nombre des dés " + playerid);
        }

        @Override
        public void onDiceCardShuffle() {
            System.out.println("sur le mélange de cartes de dés ");
        }

        @Override
        public void onLobbyComplete(List<IPlayer> playersBefore, List<IPlayer> playersAfter, Deck deck, LocalPlayerHandler localPlayerHandler) {
            System.out.println("Salon complet: ");
            System.out.println("\tjoueurs: " + playersBefore.toString());
            System.out.println("\tjoueurs: " + playersAfter.toString());

            System.out.println("À ce stade, nous devons transmettre ces données à la boucle de jeu");

            setPlayers(playersBefore, playersAfter, localPlayerHandler);
        }

        @Override
        public void onFailure(Throwable e) {
            System.out.println("en cas d'échec: " + e.getMessage());

            e.printStackTrace();
        }
    };
}
