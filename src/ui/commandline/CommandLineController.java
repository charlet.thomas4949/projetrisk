package ui.commandline;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import ai.agents.Agent;
import ai.agents.RandomAgent;
import logic.Card;
import logic.move.Move;
import logic.move.Move.Stage;
import logic.state.Board;
import logic.state.Player;
import player.PlayerController;

/**
 * CommandLineController --- Allows the user to control a player from the command line.
 */
public class CommandLineController implements PlayerController {
    private Scanner reader;
    private PrintWriter writer;

    private Player player;
    private Board board;

    private Agent testingAI; // Will fill in the blanks when I want to test a particular move stage.
    boolean testing;
    Stage testingStage;

    public CommandLineController(Scanner reader, PrintWriter writer){
        this.reader = reader;
        this.writer = writer;
        this.testing = false;
        this.testingStage = null;
    }

    public void setup(Player player, Board board){
        this.player = player;
        this.board = board;
        if(testing){
            this.testingAI = new RandomAgent();
            this.testingAI.setup(player, board);
        }
    }

    public void getMove(Move move){
        if(testing && move.getStage() != testingStage){
            testingAI.getMove(move);
            return;
        }
        switch(move.getStage()){
            case CLAIM_TERRITORY:
                claimTerritory(move);
                return;
            case REINFORCE_TERRITORY:
                reinforceTerritory(move);
                return;
            case TRADE_IN_CARDS:
                tradeInCards(move);
                return;
            case PLACE_ARMIES:
                placeArmies(move);
                return;
            case DECIDE_ATTACK:
                decideAttack(move);
                return;
            case START_ATTACK:
                startAttack(move);
                return;
            case CHOOSE_ATTACK_DICE:
                chooseAttackingDice(move);
                return;
            case CHOOSE_DEFEND_DICE:
                chooseDefendingDice(move);
                return;
            case OCCUPY_TERRITORY:
                occupyTerritory(move);
                return;
            case DECIDE_FORTIFY:
                decideFortify(move);
                return;
            case START_FORTIFY:
                startFortify(move);
                return;
            case FORTIFY_TERRITORY:
                chooseFortifyArmies(move);
                return;
            default:
                return;
        }
    }

    private void claimTerritory(Move move){
        writer.println("Quel territoire souhaitez-vous revendiquer?");
        board.printBoard(writer);
        writer.print("> ");
        int territory = chooseUnclaimedTerritory();
        move.setTerritory(territory);
    }

    private void reinforceTerritory(Move move){
        int uid = move.getUID();
        writer.println("Quel territoire souhaitez-vous renforcer?");
        board.printBoard(writer);
        writer.print("> ");
        int territory = chooseAllyTerritory(uid);
        move.setTerritory(territory);
    }

    private void tradeInCards(Move move){
        boolean tradingInCards = false;
        List<Card> hand = player.getHand();
        if(Card.containsSet(hand)){
            if(hand.size() >= 5){
                writer.println("Vous devez échanger des cartes.");
                tradingInCards = true;
            }else{
                writer.println("Voulez-vous échanger des cartes? (y ou n)");
                Card.printHand(writer, hand);
                writer.print("> ");
                writer.flush();
                tradingInCards = chooseYesNo();
            }
        } 

        List<Card> toTradeIn = new ArrayList<Card>(); 
        if(tradingInCards){
            writer.println("Quelles cartes souhaitez-vous échanger? Entrez 3 cartes.");
            Card.printHand(writer, hand);
            writer.print("> ");
            writer.flush();
            boolean correct = false;
            while(!correct){
                toTradeIn = pickCards();
                if(Card.containsSet(toTradeIn)){
                    correct = true;
                }else{
                    writer.println("Sélection non valide, réessayez.");
                    Card.printHand(writer, hand);
                    writer.print("> ");
                    writer.flush();
                }
            }
        }

        move.setToTradeIn(toTradeIn);
    }

    private List<Card> pickCards(){
        boolean correct = false;
        List<Card> hand = player.getHand();
        List<Integer> picked = new ArrayList<Integer>();
        while(!correct){
            while(!reader.hasNextInt()){
                writer.print("Entrée invalide\n> ");
                writer.flush();
                reader.next();
            }
            int cardIndex = reader.nextInt();
            if(cardIndex > 0 && cardIndex <= hand.size()){
                boolean found = false;
                for(Integer i : picked){
                    if(i == cardIndex){
                        found = true;
                    }
                }
                if(!found){
                    picked.add(cardIndex);
                    writer.print("> ");
                    writer.flush();
                }else{
                    writer.print("Vous avez déjà choisi cette carte\n> ");
                    writer.flush();
                }
            }else{
                writer.print("Index de carte invalide\n> ");
                writer.flush();
            }
            if(picked.size() == 3){
                correct = true;
            }
        }
        List<Card> toTradeIn = new ArrayList<Card>();
        for(Integer i : picked){
            toTradeIn.add(hand.get(i-1));
        }
        return toTradeIn;
    }

    private void placeArmies(Move move){
        int uid = move.getUID();
        int armiesToPlace = move.getCurrentArmies();
        int extraArmies = move.getExtraArmies();
        List<Integer> matches = move.getMatches();
        writer.format("Vous avez %d armées à placer.\n", armiesToPlace + extraArmies);
        if(matches.size() == 1){
            writer.format("Ce tour-ci, vous devez placer %d armées dans %s.\n", extraArmies, board.getName(matches.get(0)));
        }else if(matches.size() > 1){
            String matchesString = String.format("Ce tour-ci, vous devez placer %d armées dans", extraArmies);
            for(int i = 0; i != matches.size()-1; ++i){
                matchesString += String.format("%s, ", board.getName(matches.get(i)));
            }
            matchesString += String.format("ou %s.\n", board.getName(matches.get(matches.size()-1)));
            writer.print(matchesString);
        }
        writer.println("Sur quel territoire aimeriez-vous placer des armées?");
        board.printBoard(writer);
        writer.print("> ");
        int territory = chooseAllyTerritory(uid);
        writer.format("Dans combien d'armées aimeriez-vous placer %s?\n", board.getName(territory));
        board.printBoard(writer);
        writer.print("> ");
        int numArmies = 0; boolean correct = false;
        while(!correct){
            writer.flush();
            while(!reader.hasNextInt()){
                writer.print("Invalid input\n> ");
                writer.flush();
                reader.next();
            }
            numArmies = reader.nextInt();
            if(numArmies >= 1){
                if(numArmies <= armiesToPlace){
                    correct = true;
                }else{
                    writer.print("Vous ne pouvez pas placer autant d'armées.\n> ");
                }
            }else{
                writer.print("Vous devez placer au moins 1 armée.\n> ");
            }
        }

        move.setTerritory(territory);
        move.setArmies(numArmies);
    }

    private void decideAttack(Move move){
        writer.println("Voulez-vous attaquer? (y ou n)");
        board.printBoard(writer);
        writer.print("> ");
        boolean attack = chooseYesNo();
        move.setDecision(attack);
    }

    private void startAttack(Move move){
        int uid = move.getUID();
        writer.println("Choisissez le territoire à partir duquel attaquer.");
        board.printBoard(writer);
        writer.print("> ");
        int ally = -1; boolean correct = false;
        while(!correct){
            writer.flush();
            ally = chooseAllyTerritory(uid);
            if(board.getArmies(ally) >= 2){
                boolean found = false;
                for(Integer i : board.getLinks(ally)){
                    if(board.getOwner(i) != uid){
                        found = true;
                    }
                }
                if(found){
                    correct = true;
                }else{
                    writer.format("Il n'y a pas d'ennemis adjacents à %s.\n> ", board.getName(ally));
                }
            }else{
                writer.format("%s n'a pas assez d'armées pour attaquer.\n> ", board.getName(ally));
            }
        }
        writer.println("Choisissez le territoire à attaquer.");
        board.printBoard(writer);
        writer.print("> ");
        List<Integer> adjacents = board.getLinks(ally);
        int enemy = -1; correct = false;
        while(!correct){
            writer.flush();
            enemy = chooseEnemyTerritory(uid);
            for(Integer i : adjacents){
                if(enemy == i){
                    correct = true;
                }
            }
            if(!correct){
                writer.format("%s n'est pas adjacent à %s.\n> ", board.getName(enemy), board.getName(ally));
            }
        }

        move.setFrom(ally);
        move.setTo(enemy);
    }

    private void chooseAttackingDice(Move move){
        int numArmies = board.getArmies(move.getFrom());
        String attackingName = board.getName(move.getFrom());
        String defendingName = board.getName(move.getTo());
        int defendingArmies = board.getArmies(move.getTo());
        writer.format("Choisissez le nombre de dés à lancer. Vous attaquez de %s qui a %d armées à %s qui a %d armées.\n> ", attackingName, numArmies, defendingName, defendingArmies);
        int numDice = -1; boolean correct = false;
        while(!correct){
            writer.flush();
            while(!reader.hasNextInt()){
                writer.print("Entrée invalide\n> ");
                writer.flush();
                reader.next();
            }
            numDice = reader.nextInt();
            if(numDice >= 1 && numDice <= 3){
                if(numArmies > numDice){
                    correct = true;
                }else{
                    writer.format("Vous ne pouvez pas lancer autant de dés lorsque vous attaquez depuis %s.\n> ", attackingName);
                }
            }else{
                writer.print("Nombre de dés non valide.\n> ");
            }
        }

        move.setAttackDice(numDice);
    }

    private void chooseDefendingDice(Move move){
        int numArmies = board.getArmies(move.getTo());
        String defendingName = board.getName(move.getTo());
        String attackingName = board.getName(move.getFrom());
        int attackingArmies = board.getArmies(move.getFrom());
        writer.format("Choisissez combien de dés lancer. Vous défendez %s qui a %d armées, contre une attaque de %s qui a %d armées.\n> ", defendingName, numArmies, attackingName, attackingArmies);
        int numDice = -1; boolean correct = false;
        while(!correct){
            writer.flush();
            while(!reader.hasNextInt()){
                writer.print("Entrée invalide\n> ");
                writer.flush();
                reader.next();
            }
            numDice = reader.nextInt();
            if(numDice >= 1 && numDice <= 2){
                if(numArmies >= numDice){
                    correct = true;
                }else{
                    writer.format("Vous ne pouvez pas lancer autant de dés en défendant %s.\n> ", defendingName);
                }
            }else{
                writer.print("Nombre de dés non valide.\n> ");
            }
        }

        move.setDefendDice(numDice);
    }

    private void occupyTerritory(Move move){
        int currentArmies = move.getCurrentArmies();
        int numDice = move.getAttackDice();
        writer.format("Choisissez le nombre d'armées avec lesquelles occuper. Il reste %d armées sur le territoire attaquant.\n> ", currentArmies);
        int numArmies = -1; boolean correct = false;
        while(!correct){
            writer.flush();
            while(!reader.hasNextInt()){
                writer.print("Entrée invalide\n> ");
                writer.flush();
                reader.next();
            }
            numArmies = reader.nextInt();
            if(numArmies >= numDice){
                if((currentArmies - numArmies) >= 1){
                    correct = true;
                }else{
                    writer.print("Vous devez laisser au moins 1 armée derrière.\n> ");
                }
            }else{
                writer.format("Vous devez occuper au moins %d armées.\n> ", numDice);
            }
        }
        move.setArmies(numArmies);
    }

    private void decideFortify(Move move){
        writer.println("Voulez-vous fortifier? (y ou n)");
        board.printBoard(writer);
        writer.print("> ");
        boolean fortifying = chooseYesNo();
        move.setDecision(fortifying);
    }

    private void startFortify(Move move){
        int uid = move.getUID();
        writer.println("Choisissez le territoire à fortifier.");
        board.printBoard(writer);
        writer.print("> ");
        int ally = -1; boolean correct = false;
        while(!correct){
            writer.flush();
            ally = chooseAllyTerritory(uid);
            if(board.getArmies(ally) >= 2){
                boolean found = false;
                for(Integer i : board.getLinks(ally)){
                    if(board.getOwner(i) == uid){
                        found = true;
                    }
                }
                if(found){
                    correct = true;
                }else{
                    writer.format("Il n'y a pas d'alliés adjacents à %s.\n> ", board.getName(ally));
                }
            }else{
                writer.format("%s n'a pas assez d'armées pour fortifier", board.getName(ally));;
            }
        }
        writer.println("Choisissez le territoire à fortifier.");
        board.printBoard(writer);
        writer.print("> ");
        List<Integer> adjacents = board.getLinks(ally);
        int fortify = -1; correct = false;
        while(!correct){
            writer.flush();
            fortify = chooseAllyTerritory(uid);
            for(Integer i : adjacents){
                if(fortify == i){
                    correct = true;
                }
            }
            if(!correct){
                writer.format("%s n'est pas adjacent à %s.\n> ", board.getName(fortify), board.getName(ally));
            }
        }

        move.setFrom(ally);
        move.setTo(fortify);
    }

    private void chooseFortifyArmies(Move move){
        int currentArmies = move.getCurrentArmies();
        writer.format("Choisissez le nombre d'armées pour vous fortifier. Le territoire fortifié a %d armées restantes.\n> ", currentArmies);
        int numArmies = -1; boolean correct = false;
        while(!correct){
            writer.flush();
            while(!reader.hasNextInt()){
                writer.print("Entrée invalide\n> ");
                writer.flush();
                reader.next();
            }
            numArmies = reader.nextInt();
            if(numArmies >= 1){
                if((currentArmies - numArmies) >= 1){
                    correct = true;
                }else{
                    writer.print("Vous devez laisser au moins 1 armée derrière vous.\n> ");
                }
            }else{
                writer.print("Vous devez fortifier avec au moins 1 armée.\n> ");
            }
        }
        move.setArmies(numArmies);
    }

    private boolean chooseYesNo(){
        boolean decision = false;
        String answer = ""; boolean correct = false;
        while(!correct){
            writer.flush();
            answer = reader.next();
            answer.toLowerCase();
            if(answer.equals("y") || answer.equals("yes")){
                decision = true;
                correct = true;
            }else if(answer.equals("n") || answer.equals("no")){
                correct = true;
            }else{
                writer.print("Entrée invalide\n> ");
                writer.flush();
            }         
        } 
        return decision;

    }

    private int chooseUnclaimedTerritory(){
        int territory = -1; boolean correct = false;
        while(!correct){
            writer.flush();
            while(!reader.hasNextInt()){
                writer.print("Entrée invalide\n> ");
                writer.flush();
                reader.next();
            }
            territory = reader.nextInt();
            if(territory >= 0 && territory < board.getNumTerritories()){
                if(board.getOwner(territory) == -1){
                    correct = true;
                }else{
                    writer.print("Ce territoire a déjà été revendiqué.\n> ");
                }
            }else{
                writer.print("Ce territoire n'existe pas.\n> ");
            }
        }
        return territory;
    }

    private int chooseAllyTerritory(int uid){
        int territory = -1; boolean correct = false;
        while(!correct){
            writer.flush();
            while(!reader.hasNextInt()){
                writer.print("Entrée invalide\n> ");
                writer.flush();
                reader.next();
            }
            territory = reader.nextInt();
            if(territory >= 0 && territory < board.getNumTerritories()){
                if(board.getOwner(territory) == uid){
                    correct = true;
                }else{
                    writer.print("Vous ne possédez pas ce territoire.\n> ");
                }
            }else{
                writer.print("Ce territoire n'existe pas.\n> ");
            }
        }
        return territory;
    }

    private int chooseEnemyTerritory(int uid){
        int territory = -1; boolean correct = false;
        while(!correct){
            writer.flush();
            while(!reader.hasNextInt()){
                writer.print("Entrée invalide\n> ");
                writer.flush();
                reader.next();
            }
            territory = reader.nextInt();
            if(territory >= 0 && territory < board.getNumTerritories()){
                if(board.getOwner(territory) != uid){
                    correct = true;
                }else{
                    writer.print("Vous êtes propriétaire de ce territoire.\n> ");
                }
            }else{
                writer.print("Ce territoire n'existe pas.\n> ");
            }
        }
        return territory;
    }
}
