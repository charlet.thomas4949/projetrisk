package logic.state;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import logic.Card;

/**
 * Board --- Stores information about the game board.
 */
public class Board {
    // Default Board
    private static int[] CONTINENT_SIZES = {12, 8, 4, 6, 2, 10, 5, 5, 5, 12, 13, 5, 6};
    private static int[] CONTINENT_VALUES = {12, 8, 4, 6, 2, 10, 5, 5, 5, 12, 13, 5, 6};
    private static String[] CONTINENT_NAMES = {"Auvergne-Rh�ne-Alpes", "Bourgogne-Franche-Compt�", "Bretagne", "Centre-Val de Loire", "Corse",
    											"Grand Est", "Hauts-deFrance", "�le-de-France", "Normandie", "Nouvelle-Aquitaine",
    											"Occitanie", "Pays de la Loire", "Provence-Alpes-C�te d'Azur"};
    private static int[][] TERRITORY_LINKS = {
    		{ 39, 71, 69, 38, 73, 74, -1, -1}, { 59, 62, 80, 60, 77, 51, 8, -1}, { 17, 23, 63, 42, 71, 58, -1, -1}, { 4, 84, 83, 5,-1, -1, -1, -1}, { 73, 38, 26, 3, -1, -1, -1, -1},
    		{ 3, 83, 20, -1,-1, -1, -1, -1}, { 42, 43, 48, 30, 26, 38, -1, -1}, { 1, 51, 55, -1,-1, -1, -1, -1}, { 31, 10, 66, -1,-1, -1, -1, -1}, { 51, 77, 89, 21, 52, -1, -1, -1},
    		{ 8, 31, 81, 34, 66, -1, -1, -1}, { 14, 48, 30, 34, 81, 82, 46, -1}, { 30, 84, 3, 5,-1, -1, -1, -1}, { 50, 61, 27, -1,-1, -1, -1, -1}, { 18, 63, 43, 48, 11, 46, -1, -1},
    		{ 16, 79, 86, 87,24, -1, -1, -1}, { 85, 79, 15, 33,-1, -1, -1, -1}, { 41, 45, 58, 36, 2, 23, 36, -1}, { 87, 23, 63, 14, 46, 24, -1, -1}, { 20, 34, -1, -1,-1, -1, -1, -1},
    		{ 19, 5, -1, -1,-1, -1, -1, -1}, { 9, 89, 58, 71, 39, 70, 52, -1}, { 29, 56, 35, -1,-1, -1, -1, -1}, { 36, 87, 19, 63, 2, 18, -1, -1}, { 15, 33, 47, 46,19, 87, -1, -1},
    		{ 90, 70, 39, -1,-1, -1, -1, -1}, { 6, 84, 4, 38,-1, -1, -1, -1}, { 13, 61, 28, 78,92, 60, 76, -1}, { 27, 61, 72, 41, 45, 91, 78, -1}, { 22, 56, 33, -1,-1, -1, -1, -1},
    		{ 34, 12, 48, 6, 84, 12, -1, -1}, { 65, 32, 85, 81, 10, 8, -1, -1}, { 64, 40, 47, 82, 31, 65, -1, -1}, { 40, 47, 24, 16, 29, -1, -1, -1}, { 10, 81, 11, 30, 19, -1, -1, -1},
    		{ 22, 56, 44, 53,50, -1, -1, -1}, { 37, 41, 17, 23,87, 86, -1, -1}, { 49, 72, 41, 36, 86, -1, -1, -1}, { 0, 69, 26, 4, 73, -1, -1, -1}, { 25, 70, 21, 71, 0, -1, -1, -1},
    		{ 33, 47, 32, 64,-1, -1, -1, -1}, { 37, 36, 18, 45, 28, 72, -1, -1}, { 2, 63, 43, 6, 69, 71, -1, -1}, { 63, 14, 48, 6, 42, -1, -1, -1}, { 56, 35, 49, 85,-1, -1, -1, -1},
    		{ 28, 41, 18, 89, 77, 91, -1, -1}, { 24, 47, 82, 11,14, 18, -1, -1}, { 24, 33, 40, 32, 82, 46, -1, -1}, { 43, 14, 11, 30,6, -1, -1, -1}, { 44, 53, 72, 37, 86, 79, 85, -1},
    		{ 13, 61, 53, 35,-1, -1, -1, -1}, { 1, 77, 9, 52,55, 7, -1, -1}, { 51, 9, 21, 70, 88, 55, -1, -1}, { 35, 49, 72, 61,50, -1, -1, -1}, { 55, 88, 67, 57,-1, -1, -1, -1},
    		{ 7, 51, 52, 88, 54, -1, -1, -1}, { 29, 22, 35, 44,-1, -1, -1, -1}, { 54, 67, -1, -1,-1, -1, -1, -1}, { 89, 21, 71, 2, 17, -1, -1, -1}, { 62, 1, -1, -1,-1, -1, -1, -1},
    		{ 80, 1, 77, 92, 27, 76, -1, -1}, { 50, 13, 27, 28, 72, 53, -1, -1}, { 59, 80, -1, -1,-1, -1, -1, -1}, { 2, 42, 43, 14,18, 23, -1, -1}, { 40, 32, 65, -1,-1, -1, -1, -1},
    		{ 64, 32, 31, -1,-1, -1, -1, -1}, { 8, 10, -1, -1,-1, -1, -1, -1}, { 57, 88, 68, -1,-1, -1, -1, -1}, { 67, 88, 90, -1,-1, -1, -1, -1}, { 0, 38, 42, 71,-1, -1, -1, -1},
    		{ 90, 25, 39, 21,52, 88, -1, -1}, { 58, 21, 39, 0, 69, 42, 2, -1}, { 53, 49, 37, 41,28, 61, -1, -1}, { 74, 0, 38, 4,-1, -1, -1, -1}, { 0, 38, 73, -1,-1, -1, -1, -1},
    		{ 92, 77, 78, 91,-1, -1, -1, -1}, { 80, 60, 27, -1,-1, -1, -1, -1}, { 75, 92, 91, 45,89, 9, 51, 1}, { 75, 92, 27, 28,-1, -1, -1, -1}, { 86, 15, 16, 85,49, -1, -1, -1},
    		{ 62, 1, 60, 76,-1, -1, -1, -1}, { 82, 11, 34, 10,31, -1, -1, -1}, { 47, 46, 11, 81,31, 32, -1, -1}, { 5, 3, 11, -1,-1, -1, -1, -1}, { 3, 12, 30, 26,-1, -1, -1, -1},
    		{ 44, 49, 79, 16,-1, -1, -1, -1}, { 37, 36, 87, 15,79, 49, -1, -1}, { 23, 18, 24, 15,86, 36, -1, -1}, { 55, 54, 67, 68,90, 70, 52, -1}, { 9, 21, 58, 45,77, -1, -1, -1},
    		{ 68, 88, 70, 25,-1, -1, -1, -1}, { 78, 28, 45, 77,75, -1, -1, -1}, { 60, 27, 78, 75,77, -1, -1, -1}
    		};
    private static String[] TERRITORY_NAMES = {"Ain", "Aisne", "Allier", "Alpes-de-Haute-Provence", "Hautes-Alpes", 
    										   "Alpes-Maritimes", "Ard�che", "Ardennes", "Ari�ge", "Aube",
    										   "Aude", "Aveyron", "Bouches-du-Rh�ne", "Calvados", "Cantal",
    										   "Charente", "Charente-Maritime", "Cher", "Corr�ze", "Corse-du-Sud",
    										   "Haute-Corse", "C�te-d'Or", "C�tes d'Armor", "Creuse", "Dordogne",
    										   "Doubs", "Dr�me", "Eure", "Eure-et-Loir", "Finist�re",
    										   "Gars", "Haute-Garonne", "Gers", "Gironde", "H�rault",
    										   "Ille-et-Vilaine", "Indre", "Indre-et-Loire", "Is�re", "Jura",
    										   "Landes", "Loir-et-Cher", "Loire", "Haute-Loire", "Loire-Atlantique",
    										   "Loiret", "Lot", "Lot-et-Garonne", "Loz�re", "Maine-et-Loire",
    										   "Manche", "Marne", "Haute-Marne", "Mayenne", "Meurthe-et-Moselle",
    										   "Meuse", "Morbihan", "Moselle", "Ni�vre", "Nord",
    										   "Oise", "Orne", "Pas-de-Calais", "Puy-de-D�me", "Pyr�n�es-Atlantiques",
    										   "Hautes-Pyr�n�es", "Pyr�n�es-Orientales", "Bas-Rhin", "Haut-Rhin", "Rh�ne",
    										   "Haute-Sa�ne", "Sa�ne-et-Loire", "Sarthe", "Savoie", "Haute-Savoie",
    										   "Paris", "Seine-Maritime", "Seine-et-Marne", "Yvelines", "Deux-S�vres",
    										   "Somme", "Tarn", "Tarn-et-Garonne", "Var", "Vaucluse",
    										   "Vend�e", "Vienne", "Haute-Vienne", "Vosges", "Yonne",
    										   "Territoire de Belfort", "Essonne", "Val-D'Oise"
    										  };
    private static int[] TERRITORY_CARDS = {10, 5, 5, 10, 10, 10, 10, 10, 1, 5, 5, 1, 1, 5, 1, 1, 1, 5, 1, 1, 1, 5, 10, 1, 10, 5, 1,
                                            1, 10, 10, 10, 5, 5, 1, 10, 10, 5, 5, 1, 5, 10, 5};
    private static int NUM_WILDCARDS = 2;

    private List<Territory> territories;
    private List<Continent> continents;

    public Board(){
        loadDefaultBoard();
    }

    protected Board(boolean testing, int[] owners, int[] armies){
        if(testing){
            loadTestBoard();
        }else{
            loadDefaultBoard();
        }
        fillTestBoard(owners, armies);
    }

//// Use these methods

    public int getNumTerritories(){
        return territories.size();
    }

    public int getOwner(int territory){
        return territories.get(territory).getOwner();
    }

    public int getArmies(int territory){
        return territories.get(territory).getArmies();
    }

    public String getName(int territory){
        return territories.get(territory).getName();
    }

    public List<Integer> getLinks(int territory){
        return territories.get(territory).getLinks();
    }

    public int getNumContinents(){
        return continents.size();
    }

    public List<Integer> getContinent(int continent){
        return Collections.unmodifiableList(continents.get(continent).getTerritories());
    }

    // Prints a representation of the board to writer, or returns the representation as a string (if writer is null)
    public String printBoard(PrintWriter writer){
        String message = "";
        for(Territory t : territories){
            if(t.getOwner() == -1){
                message += String.format("[%d-%s-Free-%d]", t.getID(), t.getName(), t.getArmies());
            }else{
                message += String.format("[%d-%s-%d-%d]", t.getID(), t.getName(), t.getOwner(), t.getArmies());
            }
        }
        message += "\n";
        if(writer != null){
            writer.print(message);
            writer.flush();
            return "";
        }else{
            return message;
        }
    }

////

    protected void claimTerritory(int tid, int uid){
        Territory t = territories.get(tid);
        t.setOwner(uid);
    }

    protected void placeArmies(int tid, int numArmies){
        Territory t = territories.get(tid);
        t.addArmies(numArmies);
    }

    public Deck getDeck(){
        Deck deck = new Deck();
        for(int i = 0; i != territories.size(); ++i){
            //Card card = new Card(i, TERRITORY_CARDS[i], getName(i));
            //deck.addCard(card);
        }
        for(int i = territories.size(); i != territories.size()+NUM_WILDCARDS; ++i){
            Card card = new Card(i, 0, "Wildcard");
            deck.addCard(card);
        }
        return deck;
    }

    protected int calculatePlayerTerritoryArmies(int uid){
        int territoryCounter = 0;
        for(Territory t : territories){
            if(t.getOwner() == uid){
                territoryCounter++;
            }
        }
        int armies = territoryCounter/3;
        if(armies < 3){
            return 3;
        }
        return armies;
    }

    protected int calculatePlayerContinentArmies(int uid){
        int armies = 0;
        for(Continent c : continents){
            boolean owned = true;
            for(Integer TID : c.getTerritories()){
                Territory t = territories.get(TID);
                if(t.getOwner() != uid){
                    owned = false;
                }
            }
            if(owned){
                armies += c.getValue();
            }
        }
        return armies;
    }

    private void loadDefaultBoard(){
        loadBoard(CONTINENT_SIZES, CONTINENT_VALUES, CONTINENT_NAMES, TERRITORY_LINKS, 8, TERRITORY_NAMES, TERRITORY_CARDS, NUM_WILDCARDS);
    }

    private void loadTestBoard(){
        int[] continentSizes = {4, 4, 4};
        int[] continentValues = {3, 4, 5};
        String[] continentNames = {"CA", "CB", "CC"};
        int[][] territoryLinks = {{ 1,  2, -1}, { 2,  3,  5}, { 3,  9, -1}, { 7, 11, -1},
                                  { 5,  6, -1}, { 6,  7, -1}, { 7, 10, -1}, {11, -1, -1},
                                  { 9, 10, -1}, {10, 11, -1}, {11, -1, -1}, {-1, -1, -1}};
        String[] territoryNames = {"T0", "T1", "T2", "T3", "T4", "T5", "T6", "T7", "T8", "T9", "T10", "T11"};
        int[] territoryCards = {10, 5, 5, 1, 10, 5, 5, 1, 10, 5, 5, 1};
        int numWildcards = 1;
        loadBoard(continentSizes, continentValues, continentNames, territoryLinks, 3, territoryNames, territoryCards, numWildcards);
    }

    private void loadBoard(int[] continentSizes, int[] continentValues, String[] continentNames, int[][] territoryLinks, int maxLinks, String[] territoryNames, int[] territoryCards, int numWildcards){
        this.territories = new ArrayList<Territory>();
        this.continents = new ArrayList<Continent>();
        int territoryCounter = 0;
        for(int i = 0; i != continentSizes.length; ++i){
            Continent newContinent = new Continent(i);
            newContinent.setValue(continentValues[i]);
            newContinent.setName(continentNames[i]);
            for(int j = 0; j != continentSizes[i]; ++j){
                int newTID = territoryCounter++;
                newContinent.addTerritory(newTID);
                Territory newTerritory = new Territory(newTID);
                newTerritory.setName(territoryNames[newTID]);
                territories.add(newTerritory);
            }
            continents.add(newContinent);
        }
        for(int i = 0; i != territoryLinks.length; ++i){
            Territory t1 = territories.get(i);
            for(int k = 0; k != maxLinks; ++k){
                int link = territoryLinks[i][k];
                if(link != -1){
                    t1.addLink(link);
                    Territory t2 = territories.get(link);
                    t2.addLink(i);
                }
            }
        }
    }

    private void fillTestBoard(int[] owners, int[] armies){
        for(int i = 0; i != owners.length; ++i){
            claimTerritory(i, owners[i]);
            placeArmies(i, armies[i]);
        } 
    }
}
